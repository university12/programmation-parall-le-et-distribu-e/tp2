#include "Chrono.cpp"
#include "linear_miller-rabin.h"
#include "util.h"
#include <iostream>
#include <ostream>

int main(int argc, char *argv[]) {

  Chrono chronometer(false);
  Chrono chronometer_read_file;

  UserParameters user_parameters = extract_parameters_from_user(argc, argv);
  validate_user_parameters(user_parameters);
  bool use_linear = user_parameters.thread_count == 0;
  user_parameters.thread_count =
      user_parameters.thread_count == 0 ? 1 : user_parameters.thread_count;

  const std::set<IntervalLine> data = read_file(user_parameters.file_path);
  chronometer_read_file.pause();
  std::cout << "file read in " << chronometer_read_file.get() << "secondes "
            << std::endl;
  std::vector<std::list<mpz_class>> results(user_parameters.thread_count);

  chronometer.resume(); // we started when interval are treated

  if (use_linear) {
    linear_run(data, results);
  } else {
    parallel_run(data, user_parameters, results);
  }
  std::vector<mpz_class> sorted_result = linear_sort(results);

  double elapsed_time = chronometer.get();
  chronometer.pause();

  print_result(sorted_result, elapsed_time);
}
