#include "util.h"
#include "Chrono.cpp"
#include "linear_miller-rabin.h"
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <gmpxx.h>
#include <iostream>
#include <numeric>
#include <omp.h>
#include <utility>

const std::string DEBUG_FILE_PATH = "./Exemples_de_fichiers/1_simple_old.txt";
const int DEFAULT_ACCURACY = 5;
const int DEBUG_NUMBER_THREAD = 2;

std::set<IntervalLine> read_file(const std::string &file_path) {
  std::fstream file;
  std::set<IntervalLine> data;
  std::string interval_separator_token = " ";

  file.open(file_path, std::ios::in);
  if (file.is_open()) {
    std::string line;
    while (getline(file, line)) {
      IntervalLine interval = split_and_convert(line, interval_separator_token);
      data.insert(interval);
      manage_duplication_in_interval(data);
    }
    file.close();
  } else {
    throw std::logic_error("cannot open specified file");
  }

  return data;
}
bool compare_sum(const IntervalLine &a, const IntervalLine &b) {
  return a.weight > b.weight;
}
std::list<IntervalLine> distribute_load(const std::set<IntervalLine> &data,
                                        int number_of_thread) {
  std::list<IntervalLine> distributed_data;

  mpz_class total_weight(0);
  for (const auto &interval : data) {
    distributed_data.push_back(interval);
    total_weight += interval.weight;
  }

  distributed_data.sort(compare_sum);
  auto iter = distributed_data.begin();
  const int max_iter = number_of_thread * 2;
  int i = 0;
  while (distributed_data.size() <= number_of_thread && i < max_iter) {
    if (iter->second - iter->first <= 2) {
      auto [first, second] = divide_in_two(*iter);
      distributed_data.push_back(first);
      distributed_data.push_back(second);
      iter =
          distributed_data.erase(iter); // this automatically increment iterator
      if (iter == distributed_data.end()) {
        iter = distributed_data.begin();
      }
    }
    i++;
  }

  mpz_class avg_weight = total_weight / number_of_thread;
  std::cout << "TOTAL WEIGHT : " << total_weight.get_str()
            << " AVG : " << avg_weight.get_str() << std::endl;
  iter = distributed_data.begin();
  int current_split_count = 0;
  int last_split_count = INT_MAX;
  i = 0;
  while (last_split_count != current_split_count && i < max_iter) {
    if (iter->weight > avg_weight) {
      if (iter->second - iter->first <= 2) {
        auto [first, second] = divide_in_two(*iter);
        distributed_data.push_back(first);
        distributed_data.push_back(second);
        iter = distributed_data.erase(iter);
        current_split_count++;
      }
    } else {
      iter++;
    }
    if (iter == distributed_data.end()) {
      iter = distributed_data.begin();
      last_split_count = current_split_count;
      current_split_count = 0;
    }
    i++;
  }
  /**
  for (const auto &number : distributed_data) {
    std::cout << number.first.get_str() << ";" << number.second.get_str()
              << " ";
  }
  std::cout << "here !!" << std::endl;
  */
  return distributed_data;
}

std::pair<IntervalLine, IntervalLine>
divide_in_two(const IntervalLine &interval) {

  const mpz_class mid = interval.first + (interval.second - interval.first) / 2;
  IntervalLine first = IntervalLine{interval.first, mid, mid - interval.first};
  IntervalLine second =
      IntervalLine{mid + 1, interval.second, interval.second - (mid + 1)};
  set_interval_weight(first);
  set_interval_weight(second);

  return std::make_pair(first, second);
}

void set_interval_weight(IntervalLine &interval) {
  const mpz_class mid = interval.first + (interval.second - interval.first) / 2;
  interval.weight =
      mid == 0 ? mpz_class(0) : mid * (interval.second - interval.first);
}

IntervalLine split_and_convert(const std::string &initial,
                               const std::string &token) {

  int index = initial.find(token);
  if (index == -1) {
    throw std::logic_error("the separator doesn't exist");
  }
  mpz_class first = mpz_class(initial.substr(0, index));
  mpz_class second = mpz_class(initial.substr(index));

  if (first > second) {
    std::swap(first, second);
  }
  IntervalLine interval = IntervalLine{first, second, second - first};
  set_interval_weight(interval);

  return interval;
}

void find_prime_numbers(const IntervalLine &data,
                        std::vector<std::list<mpz_class>> &results,
                        uint tread_index, int accuracy) {

  for (mpz_class current_number = data.first; current_number <= data.second;
       current_number++) {
    if (prob_prime(current_number, accuracy)) {
      results[tread_index].push_back(current_number);
    }
  }
}

void print_result(const std::vector<mpz_class> &results, double time_elapsed) {
  std::cout << "Result: " << std::endl;
  for (const mpz_class &number : results) {
    std::cout << number.get_str() << " ";
  }
  std::cout << std::endl;
  std::cerr << "Elapsed time: " << time_elapsed << " seconds" << std::endl;
}

void format_file_path(std::string &file_path) {
  if (file_path[0] == '"') {
    file_path = file_path.substr(1);
  }

  if (file_path[file_path.size() - 1] == '"') {
    file_path = file_path.substr(0, file_path.size() - 2);
  }
}

UserParameters extract_parameters_from_user(int argc, char *argv[]) {
  std::vector<std::string> all_args;
  if (argc > 1) {
    all_args.assign(argv + 1, argv + argc);
  } else {
    return UserParameters{DEBUG_NUMBER_THREAD, DEBUG_FILE_PATH};
  }

  const std::string thread_count_arg = "-t";
  const std::string file_path_arg = "-f";
  int thread_count = 1;
  std::string file_path;

  for (unsigned int i = 0; i < all_args.size(); i++) { // extract arguments
    if (all_args[i] == thread_count_arg) {
      thread_count = std::stoi(all_args[i + 1]);
    } else if (all_args[i] == file_path_arg) {
      file_path = all_args[i + 1];
      format_file_path(file_path);
    }
  }
  return UserParameters{thread_count, file_path};
};

void validate_user_parameters(const UserParameters &user_parameters) {
  if (user_parameters.thread_count < 0)
    throw std::logic_error(
        "Invalid number of threads: -t argument should be 0 or higher. "
        "If 0 is specified, linear program will be run. Otherwise parallel "
        "program will be run.");

  if (user_parameters.file_path.empty()) {
    throw std::logic_error(
        "Invalid data file: -f argument should specify valid data file. "
        "Provided data file path was empty.");
  }
}

void linear_run(const std::set<IntervalLine> &data,
                std::vector<std::list<mpz_class>> &results) {

  for (const auto &interval : data) {
    find_prime_numbers(interval, results, 0, DEFAULT_ACCURACY);
  }
}

void parallel_run(const std::set<IntervalLine> &data,
                  const UserParameters &user_parameters,
                  std::vector<std::list<mpz_class>> &results) {

  Chrono chronometer;
  std::list<IntervalLine> distributed_data =
      distribute_load(data, user_parameters.thread_count);
  std::vector<IntervalLine> data_vector;
  data_vector.reserve(distributed_data.size());
  for (const auto &interval : distributed_data) {
    data_vector.push_back(interval);
  }
  chronometer.pause();
  std::cout << "data distributed in " << chronometer.get() << "secondes with "
            << data_vector.size() << " part and "
            << user_parameters.thread_count << " thread" << std::endl;

  chronometer.resume();
  omp_set_dynamic(0);
  omp_set_num_threads(user_parameters.thread_count);

#pragma omp parallel default(none)                                             \
    shared(results, data_vector, DEFAULT_ACCURACY, std::cout)
  {
#pragma omp for schedule(dynamic, 1)
    for (int i = 0; i < data_vector.size(); i++) {
      // std::cout << "Thread num : " << omp_get_thread_num() << std::endl;
      find_prime_numbers(data_vector.at(i), results, omp_get_thread_num(),
                         DEFAULT_ACCURACY);
    }
  }
}

// inspired of
// https://www.geeksforgeeks.org/check-if-any-two-intervals-overlap-among-a-given-set-of-intervals/
void manage_duplication_in_interval(std::set<IntervalLine> &data) {

  bool has_duplicate;

  do {
    auto previous_interval = data.begin();
    auto starting_interval = data.begin();
    starting_interval++;
    has_duplicate = false;
    for (auto current_interval = starting_interval;
         current_interval != data.end(); current_interval++) {
      const IntervalLine previous_val = *previous_interval;
      const IntervalLine current_val = *current_interval;
      if (previous_val.second >= current_val.first) {
        const mpz_class sum =
            (current_interval->first - 1) - previous_interval->first;
        const IntervalLine truncated_interval{previous_interval->first,
                                              current_interval->first - 1, sum};
        data.erase(previous_interval);
        data.insert(truncated_interval);
        has_duplicate = true;
      }
      previous_interval++;
    }

  } while (has_duplicate);
}

std::vector<mpz_class> linear_sort(std::vector<std::list<mpz_class>> &results) {
  std::vector<mpz_class> flatten =
      std::accumulate(results.begin(), results.end(), std::vector<mpz_class>(),
                      [](std::vector<mpz_class> a, std::list<mpz_class> b) {
                        a.insert(a.end(), b.begin(), b.end());
                        return a;
                      });
  std::sort(flatten.begin(), flatten.end());
  return flatten;
}
