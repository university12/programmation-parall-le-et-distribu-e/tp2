#ifndef util_h
#define util_h
#include <gmpxx.h>
#include <list>
#include <set>
#include <string>
#include <sys/types.h>
#include <utility>
#include <vector>

/**
TO DO
// supprimer les duplication dans read_file
// classer la liste result de fin
*/
struct IntervalLine {
  mpz_class first;
  mpz_class second;
  mpz_class weight;
};

inline bool operator<(const IntervalLine &lhs, const IntervalLine &rhs) {
  return lhs.first < rhs.first;
}

struct UserParameters {
  int thread_count;
  std::string file_path;
};

void validate_user_parameters(const UserParameters &user_parameters);
void format_file_path(std::string &file_path);
std::set<IntervalLine> read_file(const std::string &file_path);

void manage_duplication_in_interval(std::set<IntervalLine> &data);

void find_prime_numbers(const IntervalLine &data,
                        std::vector<std::list<mpz_class>> &results,
                        uint tread_index, int accuracy);

void print_result(const std::vector<mpz_class> &results, double time_elapsed);
IntervalLine split_and_convert(const std::string &initial,
                               const std::string &token);
void set_interval_weight(IntervalLine &interval);

UserParameters extract_parameters_from_user(int argc, char *argv[]);
void linear_run(const std::set<IntervalLine> &data,
                std::vector<std::list<mpz_class>> &results);

void parallel_run(const std::set<IntervalLine> &data,
                 const UserParameters &user_parameters,
                 std::vector<std::list<mpz_class>> &results);

std::vector<IntervalLine> distribute_load(const std::set<mpz_class> &data,
                                       int number_of_thread);
std::pair<IntervalLine, IntervalLine>
divide_in_two(const IntervalLine &interval);

bool compare_sum(const IntervalLine &a,const IntervalLine &b);

std::vector<mpz_class> linear_sort(std::vector<std::list<mpz_class>> &results);
#endif