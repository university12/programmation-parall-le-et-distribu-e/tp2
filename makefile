.PHONY: build clear

build :
	mkdir -p build
	cd build && cmake ./.. && make
	mv ./build/compile_commands.json ./compile_commands.json
	make debug-build

debug-build :
	mkdir -p debug
	cd debug && cmake  -DCMAKE_BUILD_TYPE=Debug ./.. && make
clear:
	rm -rf ./build
	rm -rf ./debug
run:
	./build/tp2
install-depency:
	apt-get libgmp3-dev
