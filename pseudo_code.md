algo_tp2(file_path, number_of_thread)
{
	data[] <- read_file_as_interval(file_path,number_of_thread)
 	result[number_of_thread][]
    crono.start()
    distribute_data(data)
    #pragma omp for schedule(dynamic, 1)
    for (interval in data)
    {
    	find_prime_number(interval,thread_num,result)
    }
  	crono.stop()
    sorted_result[]<- sort(result)
    print(sorted_result)
    print(crono.get)

}

manage_duplicate(file_path)
{
	has_duplicate<- false
 	previous_interval = data[0]
    for(interval in data[1:])
    {
    	if(interval.second>=previous_interval.first)
        {
        	truncated_interval<-{previous_interval.first,interval.second,interval.second-previous_interval.first*mean(previous_interval.first,interval.second)}
          delete interval from data
           add truncated_interval into data
            has_duplicate<-true
        }
    }
}

distribute_data(data,number_of_thread)
{
	avg_weight <- avg(data,weight)// calculate the average of the weight property
    sort(data,weight) // sort the data by the weight property
    max_iter <- number_of_thread*2
    iter <- data.begin()
    i <- 0
    while(data.size()<=number_of_thread && i<max_iter)
      {
      	fist,second <- divide_interval_in_two(iter) // two egal part
        data.add(first,second)
        delete iter inside data
        
         if(iter==data.end())
         {
         	iter<- data.begin()
         }
        else
         {
        	iter++
        }
      }
    iter <- data.begin()
  	i <- 0
    while(data.size()<=number_of_thread && i<max_iter)
    {
    	if(iter.weight>avg_weight)
        {
        	fist,second <- divide_interval_in_two(iter) // two egal part
        data.add(first,second)
        delete iter inside data
        
         if(iter==data.end())
         {
         	iter<- data.begin()
         }
        else
         {
        	iter++
         }
     	}
    }
}